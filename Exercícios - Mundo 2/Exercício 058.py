# Jogo da adivinhação v2.0
from random import randint
numPensado = randint(0, 10)
num = int(input('Pensei em um número entre 0 e 10. Consegue adivinhar? '))
tentativas = 0
while num != numPensado:
    tentativas += 1
    num = int(input('Em que número eu pensei? '))
print('Parabéns! Você acertou!!! :)' if tentativas == 0 else 'Você acertou, depois de {} tentativas.'.format(tentativas))
