# Clássico da média
n1 = float(input('Digite a primeira nota: '))
n2 = float(input('Digite a segunda nota: '))
media = (n1 + n2) / 2
if media < 5.0:
    resultado = "Reprovado"
elif 5.0 <= media < 7.0:
    resultado = "Recuperação"
else:
    resultado = "Aprovado"
print('Resultado final: {}'.format(resultado))
