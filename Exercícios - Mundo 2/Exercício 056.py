# Analisador completo
mediaIdade = 0.0
nomeVelho = ''
qtdM20 = 0
mIdade = 0
for i in range(1, 5):
    nome = input('Digite o nome da {}ª pessoa [a-zA-Z]: '.format(i))
    idade = int(input('Digite a idade da {}ª pessoa [1-100]: '.format(i)))
    sexo = input('Digite o sexo da {}ª pessoa [M/F]: '.format(i)).upper()
    mediaIdade += idade / 4
    if idade > mIdade and sexo == 'M':
        nomeVelho = nome
        mIdade = idade
    if sexo == 'F' and idade < 21: qtdM20 += 1
print('''Média de idade do grupo: {}
Mome do homem mais velho: {}
Quantidade de mulheres com menos de 20 anos: {}'''.format(mediaIdade, nomeVelho, qtdM20))
