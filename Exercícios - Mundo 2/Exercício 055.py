# Maior e menor na sequência
peso = float(input('Digite o peso da pessoa: '))
maior, menor = peso, peso
for i in range(0, 4):
    peso = float(input('Digite o peso da pessoa: '))
    if peso > maior: maior = peso
    elif peso < menor: menor = peso
print('Maior peso: {}, Menor peso: {}'.format(maior, menor))
