# Jogo do par ou ímpar
from random import randint
qtdV = 0
while True:
    num = int(input('Digite seu número da sorte: '))
    pi = input('Você quer Par ou Ímpar? [P/I] ').strip().upper()
    numPC = randint(0, 10)
    jogo = num + numPC
    if 'P' in pi:
        if jogo % 2 == 0:
            print(f'Parabéns! Você venceu a partida. Resultado: {jogo}')
            qtdV += 1
        else:
            print(f'Você perdeu. Resultado: {jogo}')
            break
    else:
        if jogo % 2 == 0:
            print(f'Você perdeu. Resultado: {jogo}')
        else:
            print(f'Parabéns! Você venceu a partida. Resultado: {jogo}')
            qtdV += 1
print(f'Você ganhou {qtdV} vezes consecutivas.')
