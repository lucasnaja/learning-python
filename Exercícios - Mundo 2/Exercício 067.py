# Tabuada v3.0
while True:
    num = int(input('Digite um número: '))
    if num < 0:
        break
    for i in range(1, 11):
        print(f'{num} x {i:2} = {num * i}')
