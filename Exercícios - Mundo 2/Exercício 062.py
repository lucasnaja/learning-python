# Super progressão aritmética v3.0
a1 = float(input('Digite o primeiro termo da progressão: '))
r = float(input('Digite a razão da PA: '))
contagem = int(input('Quer ver quantos termos? '))
while contagem != 0:
    cont = 0
    while cont < contagem:
        print('{} ->'.format(a1 + r * cont), end=' ')
        cont += 1
    print('ACABOU')
    contagem = int(input('\nQuer ver quantos termos agora? '))