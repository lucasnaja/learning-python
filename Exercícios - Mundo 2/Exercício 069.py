# Análise de dados do grupo
qtdP18 = qtdH = qtdMM20 = 0
while True:
    idade = int(input('Digite a idade da pessoa: ').strip())
    sexo = input('Digite o Sexo da pessoa: [M/F] ').strip().upper()
    if idade < 18:
        qtdP18 += 1
    if 'M' in sexo:
        qtdH += 1
    if 'M' in sexo and idade < 20:
        qtdMM20 += 1
    con = input('Quer continuar: [S/N] ').upper().strip()
    if 'N' in con:
        break
print('Quantidade de pessoas com menos de 18 anos: {}'.format(qtdP18))
print(f'Quantidade de homens: {qtdH}')
print(f'Quantidade de mulheres com menos de 20 anos: {qtdMM20}')
