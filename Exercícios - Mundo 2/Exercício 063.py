# Sequência de Fibonacci
n = int(input('Digite o n elementos da sequência de fibonacci: '))
a, b = 0, 1
print('{} {}'.format(a, b), end=' ')
while n > 2:
    c = a + b
    print('{}'.format(c), end=' ')
    a, b = b, c
    n -= 1
