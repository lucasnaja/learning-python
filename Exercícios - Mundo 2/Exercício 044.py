# Gerenciador de pagamentos
pProd = float(input('Digite o preço do produto: '))
print('à vista no [1, dinheiro, cheque]: 10% de desconto')
print('à vista no [2, cartão]: 5% de desconto')
print('em até 2x no [3, 2cartão]: preço normal')
print('3x ou mais no [4, 3cartão]: 20% de juros')
mPagamento = input('Digite o método de pagamento: ').strip().lower()
if mPagamento == 'dinheiro' or mPagamento == 'cheque' or '1' == mPagamento:
    print('Preço original: R${:.2f}, preço com 10% descontado: {:.2f}'.format(pProd, pProd - (pProd * 0.10)))
elif mPagamento == 'cartão' or mPagamento == '2':
    print('Preço original: R${:.2f}, preço com 5% descontado: {:.2f}'.format(pProd, pProd - (pProd * 0.05)))
elif mPagamento == '2cartão' or mPagamento == '3':
    print('Preço: R${:.2f}'.format(pProd))
elif mPagamento == '3cartão' or mPagamento == '4':
    print('Preço original: R${:.2f}, preço com 20% de juros: R${:.2f}'.format(pProd, pProd + (pProd * 0.20)))

