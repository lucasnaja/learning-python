# Cálculo IMC
peso = float(input('Digite seu peso: '))
altura = float(input('Digite sua altura: ').replace(',', '.'))
print('Seu peso: {}kg'.format(peso))
print('Sua altura: {}m'.format(altura))
imc = peso / altura ** 2
if imc < 18.5: resultado = 'Abaixo do peso'
elif 18.5 < imc < 25: resultado = 'Peso ideal'
elif 25 <= imc < 30: resultado = 'Sobrepeso'
elif 30 <= imc < 40: resultado = 'Obesidade'
else: resultado = 'Obesidade mórbida'
print('Você possui um IMC de {} e seu resultado é {}'.format(imc, resultado))
