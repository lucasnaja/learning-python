# Pedra, Papel, Tesoura
from random import randint

coresJogadas, coresResultado, fechaCor = ['\033[1;33m', '\033[1;35m', '\033[1;34m'], ['\033[1;32m', '\033[1;31m', '\033[1;36m'], '\033[m'
material = ['Pedra', 'Papel', 'Tesoura']
jogada, jogPC = str(input('{0}Pedra{3}, {1}Papel{3} ou {2}Tesoura{3}? '.format(coresJogadas[0], coresJogadas[1], coresJogadas[2], fechaCor))).strip().lower(), randint(0, len(material) - 1)

if jogada == 'pedra': jog = 0
elif jogada == 'papel': jog = 1
elif jogada == 'tesoura': jog = 2

if jog == 0 and jogPC == 0: resultado = 'EMPATE'
elif jog == 0 and jogPC == 1: resultado = 'DERROTA'
elif jog == 0 and jogPC == 2: resultado = 'VITÓRIA'
elif jog == 1 and jogPC == 0: resultado = 'VITÓRIA'
elif jog == 1 and jogPC == 1: resultado = 'EMPATE'
elif jog == 1 and jogPC == 2: resultado = 'DERROTA'
elif jog == 2 and jogPC == 0: resultado = 'DERROTA'
elif jog == 2 and jogPC == 1: resultado = 'VITÓRIA'
elif jog == 2 and jogPC == 2: resultado = 'EMPATE'

print('Você jogou: {}{}{}'.format(coresJogadas[jog], material[jog], fechaCor))
print('Computador jogou: {}{}{}'.format(coresJogadas[jogPC], material[jogPC], fechaCor))
if resultado == 'VITÓRIA': cor = 0
elif resultado == 'DERROTA': cor = 1
elif resultado == 'EMPATE': cor = 2
print('Resultado: {}{}{}'.format(coresResultado[cor], resultado, fechaCor))