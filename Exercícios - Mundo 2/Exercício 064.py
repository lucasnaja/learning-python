# Tratando vários valores
num = int(input('Digite um valor: '))
cont = 1 if num != 999 else 0
soma = num if num != 999 else 0
while num != 999:
    num = int(input('Digite um valor: '))
    cont += 1 if num != 999 else 0
    soma += num if num != 999 else 0
print('Foram digitados {} números, e a soma deles é de {}'.format(cont, soma))
