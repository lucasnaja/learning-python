# Conversor de bases numéricas
num = int(input('Digite um número aleatório: '))
print('[ 1 ] Para converter para BINÁRIO')
print('[ 2 ] Para converter para OCTAL')
print('[ 3 ] Para converter para HEXADECIMAL')
esc = input('Sua opção: ').strip()
if esc == '1':
    resultado = bin(num)[2:]
elif esc == '2':
    resultado = oct(num)[2:]
elif esc == '3':
    resultado = hex(num)[2:]
else: resultado = 'Opção inválida.'
print('Resultado: {}'.format(resultado))
