# Alistamento militar
from datetime import date
anoNasc = int(input('Digite o ano de nascimento: '))
anoAtual = date.today().year
idade = anoAtual - anoNasc
if idade < 18:
    print('Você ainda irá se alistar. Falta {} anos para isso!'.format(18 - idade))
elif idade == 18:
    print('Já é hora de se alistar! Boa sorte. ;)')
else:
    print('Já passou do tempo de se alistar. {} anos, exatamente.'.format(idade - 18))
