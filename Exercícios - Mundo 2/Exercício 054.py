# Grupo da maioridade
from datetime import date
mI, meI = 0, 0
for i in range(0, 7):
    data = int(input('Digite sua data de nascimento: ').split('/')[2])
    if date.today().year - data < 21:
        meI += 1
    else: mI += 1
print('Há {} pessoas menores de idade, e {} pessoas maiores de idade.'.format(meI, mI))
