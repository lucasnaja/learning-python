# Cálculo do fatorial
num = int(input('Digite um número: '))
fat = num
fatorial = 1
while num > 1:
    fatorial *= num
    num -= 1
print('{}! = {}'.format(fat, fatorial))
