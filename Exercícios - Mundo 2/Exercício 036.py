# Aprovando empréstimo
valorCasa = float(input('Digite o valor da casa: R$'))
salario = float(input('Digite o salário do cliente: R$'))
anos = int(input('Digite em quantos anos a pessoa irá pagar: '))

media = valorCasa / (anos * 12)
pSal = salario * (30/100)
if media > pSal:
    print('O cliente não pode comprar esta casa. Média: R${:.2f}'.format(media))
    while media > pSal:
        anos += 1
        media = valorCasa / (anos * 12)
        pSal = salario * (30 / 100)
    print('O cliente poderá comprar esta casa se for {} anos.'.format(anos))
else:
    print('O cliente pode comprar esta casa pagando R${:.2f} por mês.'.format(media))
