# Classificando atletas
from datetime import date
anoNasc = int(input('Digite seu ano de nascimento: '))
idade = date.today().year - anoNasc
if idade <= 9:
    categoria = "Mirim"
elif idade <= 14:
    categoria = "Infantil"
elif idade <= 19:
    categoria = "Junior"
elif idade <= 20:
    categoria = "Sênior"
else:
    categoria = "Master"
print('Categoria: {}'.format(categoria))
