# Detector de palíndromo
frase = input('Digite a frase: ').strip().replace(' ', '')
novaFrase = ''
for i in range(len(frase), 0, -1):
    novaFrase += frase[i - 1]
print('É palíndroma' if novaFrase == frase else 'Não é palíndroma')
