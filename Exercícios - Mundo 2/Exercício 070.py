# Estatísticas em produtos
tGasto = pM1000 = pMB = pMBI = 0
while True:
    nome = input('Digite o nome do produto: ').strip()
    preco = float(input('Digite o preço do produto: ').strip())
    if tGasto == 0:
        pMB = nome
        pMBI = preco
    tGasto += preco
    if preco > 1000:
        pM1000 += 1
    if preco < pMBI:
        pMBI = preco
        pMB = nome
    cond = input('Deseja continuar: [S/N] ').strip().upper()
    if not 'S' in cond:
        break
print(f'Total gasto na conta: {tGasto}')
print(f'Quantidade de produtos mais caros que R$1000.00: {pM1000}')
print(f'Produto mais barato: {pMB}')
