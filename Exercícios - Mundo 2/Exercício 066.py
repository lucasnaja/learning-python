# Vários números com flag
qtdNum = soma = 0
while True:
    num = int(input('Digite um número: '))
    if num == 999:
        break;
    qtdNum += 1
    soma += num
print(f'Quantidade de números digitados: {qtdNum}\nSoma de todos os números: {soma}')
