# Menu de opções
num1 = float(input('Digite um número: '))
num2 = float(input('Digite outro número: '))
num = 0
while num != 5:
    print('''[1] Somar
[2] Multiplicar
[3] Maior número
[4] Novos números
[5] Sair''')
    num = int(input('Opção: '))
    if num == 1:
        print('Soma entre {} e {} = {}'.format(num1, num2, num1 + num2))
    elif num == 2:
        print('Multiplicação entre {} e {} = {}'.format(num1, num2, num1 * num2))
    elif num == 3:
        print('Maior número: {}'.format(num1) if num1 > num2 else 'Maior número: {}'.format(num2))
    elif num == 4:
        num1 = float(input('Digite um número: '))
        num2 = float(input('Digite outro número: '))
print('FIM DE EXECUÇÃO')