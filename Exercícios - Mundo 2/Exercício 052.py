# Números primos
num = int(input('Digite um número qualquer: '))
s = 0
for i in range(num, 0, -1):
    if num % i == 0:
        s += 1
print('Número primo' if s == 2 else 'Número não é primo')
