# Simulador de caixa eletrônico
valor = float(input('Digite o valor a ser sacado: '))
c50 = c20 = c10 = c1 = 0
if valor >= 50:
    c50 = valor // 50
    valor -= c50 * 50
if valor >= 20:
    c20 = valor // 20
    valor -= c20 * 20
if valor >= 10:
    c10 = valor // 10
    valor -= c10 * 10
if valor >= 1:
    c1 = valor // 1
    valor -= c1 * 1
print('Notas de 50: {}'.format(int(c50)))
print('Notas de 20: {}'.format(int(c20)))
print(f'Notas de 10: {int(c10)}')
print(f'Notas de 1: {int(c1)}')
print(f'Restante: R${valor:.2f}')
