# Maior e menor valores
valor = int(input('Digite o valor: '))
qtd, soma = 1, valor
c = input('Deseja continuar? [S/N] ').strip().upper()
menor = maior = valor
while 'S' in c:
    valor = int(input('Digite o valor: '))
    qtd += 1
    soma += valor
    if valor > maior: maior = valor
    elif valor < menor: menor = valor
    c = input('Deseja continuar? [S/N] ').strip().upper()
print('Média de todos os valores digitados: {}'.format(soma / qtd))
print('Menor valor digitado: {}'.format(menor))
print('Maior valor digitado: {}'.format(maior))
