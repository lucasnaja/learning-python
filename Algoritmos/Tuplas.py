def join(array, separator):
    size = len(array)
    text = ''
    for i in range(0, size):
        text += '{}{}'.format(array[i], "" if i == size - 1 else separator)

    return text


lanches = ('Hamburguer', 'Suco', 'Maçã', 'Pêra', 'Ameixa')
print(join(lanches, "|"))
