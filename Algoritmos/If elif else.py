name = input('Digite seu nome: ')

if name == 'Lucas':
    print('Seu nome é Lucas!')
elif name == 'Fernando':
    print('Seu nome é Fernando!')
elif name == 'Bárbara':
    print('Seu nome é Bárbara!')
else:
    print(f'Seu nome é {name}')

# if condição:
#     operação

# if condição and condição2 or condição3 and not condição4 or not condição5:
#     operação
