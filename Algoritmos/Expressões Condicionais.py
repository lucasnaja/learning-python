from random import randint
rndNumber = randint(0, 10)

inputNumber = int(input('Digite um número: '))

print(f'Número aleatório: {rndNumber}\Número digitado: {inputNumber}')

print(f"Você {'errou' if rndNumber != inputNumber else 'acertou'}!")
# print(f'Você {rndNumber != inputNumber and "errou" or "acertou"}!')

# <expressao1> if <condicao> else <expressao2>
# <condicao> and <expressao1> or <expressao2>
