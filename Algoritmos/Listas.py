def join(array, separator):
    size = len(array)
    text = ''
    for i in range(0, size):
        text += '{}{}'.format(array[i], "" if i == size - 1 else separator)

    return text


lanches = ['Hamburguer', 'Suco', 'Maçã', 'Pêra', 'Ameixa']
lanches.insert(0, 'dkjfjsd')
del lanches[0]
lanches.pop()
if 'Hamburguer' in lanches:
    lanches.remove('Hamburguer')

print(join(lanches, ", "))

valores = list(range(1, 11))
valores.sort(reverse=True)
print(valores)
