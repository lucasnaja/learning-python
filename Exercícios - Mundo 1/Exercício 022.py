# Analisador de textos
nome = input('Digite seu nome completo: ')
print('Analisando seu nome...')
print('Seu nome em maiúsculo é: {}'.format(nome.upper()))
print('Seu nome em minúsculas é: {}'.format(nome.lower()))
print('Seu nome ao todo tem {} letras'.format(len(nome.replace(' ', ''))))
print('Seu primeiro nome é {} e tem {} letras'.format(nome.split()[0], len(nome.split()[0])))