# Custo da viagem
km = float(input('Digite o tamanho da viagem: '))
cot = 0.5 if km <= 200 else 0.45
print('O preço de sua viagem é de R${:.2f}'.format(km * cot))