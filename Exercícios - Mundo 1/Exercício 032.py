# Ano bissexto
from datetime import date
ano = date.today().year
#ano = int(input('Digite o ano: '))
if ano % 400 == 0 or ano % 4 == 0 and ano % 100 != 0:
    print('Ano {} é bissexto!'.format(ano))
else:
    print('Ano {} não é bissexto!'.format(ano))
