# Primeira e última ocorrência de uma string
nome = input('Digite a frase: ').lower().strip()
qtdA = nome.count('a')
primeiroA = nome.find('a')
ultimoA = nome.rfind('a')
#ultimoA = 0
#for i in range(primeiroA, len(nome)):
#    if nome[i] == 'a':
#        ultimoA = i
print('Quantidade de A\'s: {}\nPrimeiro A: {}\nÚltimo A: {}'.format(qtdA, primeiroA + 1, ultimoA + 1))
