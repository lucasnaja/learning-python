# Aluguel de carros
dias = float(input('Digite os dias que cliente usou o carro: '))
kmR = float(input('Digite os KM\'s rodados pelo carro: '))
Pagar = dias * 60 + kmR * 0.15
print('O carro do cliente rodou {}KM por {:.0f} dias. O preço a pagar é de R${:.2f}'.format(kmR, dias, Pagar))