# Jogo da Adivinhação v.1.0
from time import sleep
from random import randint
corAmarela, corAzul, corVermelha, corRoxa, fechaCor = '\033[33m', '\033[34m', '\033[31m', '\033[1;35m', '\033[m'
print('{}-=-{}'.format(corAmarela, fechaCor) * 20)
print('{}Vou pensar em um número entre 0 e 5. Tente adivinhar{}'.format(corAzul, fechaCor))
print('{}-=-{}'.format(corAmarela, fechaCor) * 20)
num = int(input('Em que número eu pensei? '))
print('{}PROCESSANDO...{}'.format(corRoxa, fechaCor))
sleep(2)
numPensado = randint(0, 5)
print('{}PARABÉNS! Você conseguiu me vencer!{}'.format(corAmarela, fechaCor) if num == numPensado else '{}GANHEI! Eu pensei no número {} e não no {}!{}'.format(corVermelha,numPensado, num, fechaCor))
