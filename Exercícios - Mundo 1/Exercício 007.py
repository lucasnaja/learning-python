# Média aritmética
n1 = float(input('Digite o primeiro número: '))
n2 = float(input('Digite o segundo número: '))
print('A média aritmética entre {:.1f} e {:.1f} = {:.1f}'.format(n1, n2, (n1 + n2) / 2))
