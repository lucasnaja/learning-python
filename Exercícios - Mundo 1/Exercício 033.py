# Maior e menor valores
n1 = float(input('Digite o primeiro valor: '))
maior, menor = n1, n1
n2 = float(input('Digite o segundo valor: '))
if n2 > maior: maior = n2
elif n2 < menor: menor = n2
n3 = float(input('Digite o terceiro valor: '))
if n3 > maior: maior = n3
elif n3 < menor: menor = n3
print('Menor valor: {}\nMaior valor: {}'.format(menor, maior))
