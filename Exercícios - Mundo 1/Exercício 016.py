# Quebrando um número
from math import trunc
value = float(input('Digite um número: '))
print('O número digitado foi {} e seu valor inteiro é {}'.format(value, trunc(value)))