# Conversor de temperaturas
tempC = float(input('Digite a temperatura em C°: '))
tempF = tempC * 1.8 + 32
print('Temperatura de {}C° para F = {}'.format(tempC, tempF))
tempK = tempC + 273.15
print('Temperatura de {}C° para K = {}'.format(tempC, tempK))