# Seno, Cosseno e Tangente
from math import radians, sin, cos, tan
ang = float(input('Digite o ângulo a calcular: '))
sen = sin(radians(ang))
cos = cos(radians(ang))
tan = tan(radians(ang))
print('Em um ângulo de {}° o Seno é {:.2f}, Cosseno é {:.2f}, Tangente é {:.2f}'.format(ang, sen, cos, tan))