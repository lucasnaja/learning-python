# Analisador de triângulos
l1 = float(input('Digite o primeiro lado do triângulo: '))
l2 = float(input('Digite o segundo lado do triângulo: '))
l3 = float(input('Digite o terceiro lado do triângulo: '))
tri = (l1 - l2) < l3 < (l1 + l2) and (l1 - l3) < l2 < (l1 + l3) and (l3 - l2) < l1 < (l3 + l2)
print('É triângulo? {}'.format(tri))
