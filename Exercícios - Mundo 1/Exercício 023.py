# Unidades, Dezenas, Centenas e Milhares
num = int(input('Digite um número: '))
milhar, centena, dezena, unidade = 0, 0, 0, 0
if num >= 1000:
    milhar = num // 1000
    num -= milhar * 1000
if num >= 100:
    centena = num // 100
    num -= centena * 100
if num >= 10:
    dezena = num // 10
    num -= dezena * 10
if num >= 1:
    unidade = num // 1
    num -= unidade * 1
print('Unidades: {}\nDezenas: {}\nCentenas: {}\nMilhares: {}'.format(unidade, dezena, centena, milhar))
