# Calculando descontos
pr = float(input('Digite o preço do produto: '))
print('O produto custa R${:.2f} e com desconto de 5% ficará R${:.2f}'.format(pr, pr - (5 / 100) * pr))