# Dobro, triplo, raíz quadrada
num = int(input('Digite um número: '))
print('Número: {}, dobro: {}, triplo: {}, raíz quadrada: {}'.format(num, num*2, num*3, num**(1/2)))