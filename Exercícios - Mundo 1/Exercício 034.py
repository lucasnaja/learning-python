# Aumentos múltiplos
sal = float(input('Digite o salário do funcionário: '))
cot = 0.15 if sal <= 1250 else 0.10
print('O preço do salário após o aumento é {}'.format(sal + sal * cot))