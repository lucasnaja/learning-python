# Par ou Ímpar
num = int(input('Me diga um número qualquer: '))
print('{} é par'.format(num) if num % 2 == 0 else '{} é ímpar'.format(num))