# Radar eletrônico
vel = float(input('Digite a velocidade atual do carro: '))
if vel > 80:
    multa = (vel - 80) * 7
    print('Sua velocidade é de {}KM/h e sua multa é de R${:.2f}'.format(vel, multa))
else:
    print('Tenha um bom dia! =)')
