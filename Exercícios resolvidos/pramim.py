VA, qn = map(str, input().split())
qn = int(qn)
tA = 0
tV = 0

for i in range(0, qn):
    qd = int(input())
    if (VA == 'V'):
        tV += qd
        VA = 'A'
    else:
        tA += qd
        VA = 'V'

print('VOCE: {} AMIGO: {}'.format(tV, tA))
