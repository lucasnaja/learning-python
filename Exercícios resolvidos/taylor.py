from math import floor, ceil
t = 0


def fact(num):
    resp = 1
    for i in range(num, 0, -1):
        resp *= i

    return resp


X = int(input())
for n in range(0, 5):
    R = X * 3.1415 / 180
    t += ((-1) ** n * R ** (2 * n)) / fact(2 * n)

v = int(t * 10000)
B = int(t * 1000)
B *= 10

C = v - B

if C >= 0 and C <= 6:
    v = v / 10
    t = (int(floor(v))) / 1000
else:
    v = v / 10
    t = (int(ceil(v))) / 1000

print('{:.3f}'.format(t))
