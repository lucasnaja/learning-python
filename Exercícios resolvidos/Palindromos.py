word = input('Digite uma palavra: ').replace(' ', '').lower()
wordReversed = ''

for i in range(len(word) - 1, -1, -1):
    wordReversed += word[i]

print(f'A palavra {"não " if word != wordReversed else ""}é um palíndromo')
