qtd = int(input())
qtdZ = 0
qtdU = 0
qtdD = 0
qtdT = 0
qtdQ = 0
qtdC = 0
qtdS = 0
qtdSe = 0
qtdO = 0
qtdN = 0


def primo(num):
    qtd = 2

    for i in range(num - 1, 1, -1):
        if num % i == 0:
            qtd += 1
            break

    if num == 1:
        return False
    elif qtd == 2:
        return True
    else:
        return False


for i in range(0, qtd):
    n1, n2 = map(int, input().split())
    for j in range(n1, n2):
        if (primo(j)):
            qtdZ += str(j).count('0')
            qtdU += str(j).count('1')
            qtdD += str(j).count('2')
            qtdT += str(j).count('3')
            qtdQ += str(j).count('4')
            qtdC += str(j).count('5')
            qtdS += str(j).count('6')
            qtdSe += str(j).count('7')
            qtdO += str(j).count('8')
            qtdN += str(j).count('9')

    print('INTERVALO {}'.format(i + 1))
    print('0: {}'.format(qtdZ))
    print('1: {}'.format(qtdU))
    print('2: {}'.format(qtdD))
    print('3: {}'.format(qtdT))
    print('4: {}'.format(qtdQ))
    print('5: {}'.format(qtdC))
    print('6: {}'.format(qtdS))
    print('7: {}'.format(qtdSe))
    print('8: {}'.format(qtdO))
    print('9: {}'.format(qtdN))
    qtdZ = 0
    qtdU = 0
    qtdD = 0
    qtdT = 0
    qtdQ = 0
    qtdC = 0
    qtdS = 0
    qtdSe = 0
    qtdO = 0
    qtdN = 0
