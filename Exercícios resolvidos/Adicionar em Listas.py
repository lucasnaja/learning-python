resp = 'S'
lista = []

while resp == 's' or resp == 'S':
    num = int(input('Digite um número: '))

    if num in lista:
        print('Número duplicado! Nâo vou adicionar.')
    else:
        lista.append(num)
        print(f'{num} foi adicionado com sucesso!')

    resp = input('Deseja continuar? [S/N]: ')

print(f'Sua lista: {lista}')
