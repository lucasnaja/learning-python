qtd = int(input())
me = 1439
ma = 0

for i in range(0, qtd):
    h1, m1, h2, m2 = map(int, input().split())
    p1 = h1 * 60 + m1
    p2 = h2 * 60 + m2

    if p1 < me:
        me = p1

    if p2 > ma:
        ma = p2

print(ma - me)
