qtd = int(input())
danette = []
silvio = []
pD = 0
pS = 0
empate = 0

for i in range(0, qtd):
    danette.append(input().split())

danette.sort()

for i in range(0, qtd):
    silvio.append(input().split())

silvio.sort()

for i in range(0, qtd):
    for j in range(0, 4):
        pd = int(danette[i][j])
        ps = int(silvio[i][j])

        if pd > ps:
            pD += 1
            break
        elif pd < ps:
            pS += 1
            break

        if j == 3:
            empate += 1

print('danette venceu: {}'.format(pD))
print('silvio venceu: {}'.format(pS))
print('empates: {}'.format(empate))
